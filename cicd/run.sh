#!/bin/sh
echo "=========== Run Script ========================"

APP_VERSION="0.3.0"
APP_URL="https://gitlab.com/api/v4/projects/55804318/packages/maven/com/gcttc/gcttc-rankings/${APP_VERSION}/gcttc-rankings-${APP_VERSION}.jar"

echo "Downloading ${APP_URL}"
curl --fail-with-body --output app.jar "${APP_URL}"

echo "Directory files:"
ls

echo "Executing Ranking calculation"
java -jar app.jar || exit

echo "Result files"
ls -l data/output || exit

echo "Result file content (first 25 lines):"
jq '.' data/output/rankings.json | head -n 25 || exit

echo "Result file ranking count:"
jq '.rankings | length' data/output/rankings.json || exit

echo "=========================================================="