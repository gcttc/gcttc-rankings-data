# Guayaquil City Table Tennis Club - Rankings Data

<!-- TOC -->
* [Guayaquil City Table Tennis Club - Rankings Data](#guayaquil-city-table-tennis-club---rankings-data)
  * [Overview](#overview)
  * [How to use it](#how-to-use-it)
  * [How it works](#how-it-works)
  * [Additional details](#additional-details)
    * [Input File formats](#input-file-formats)
      * [Initial Ranking file](#initial-ranking-file)
      * [Tournament file](#tournament-file)
    * [Output File formats](#output-file-formats)
      * [Ranking Table JSON file](#ranking-table-json-file)
<!-- TOC -->
<br/>

> This repository is one of a collection of three that support this solution:
>
> - The [GCTTC Rankings Tool] repository: The program used for the calculation.
> - The [GCTTC Rankings Data] repository: The initial and tournament reported data.
> - The [GCTTC Rankings Web] repository: The calculated data presentation web application.

## Overview

This is the repository of the **GCTTC Rankings Data**. This is where all the tournament data is maintained by [Guayaquil City Table Tennis Club].

Every time you update this information the pipeline starts updating the visualization data published on The [GCTTC
Rankings Website](https://gitlab.com/gcttc/gcttc-rankings-web.git)

## How to use it

TBD

## How it works

TBD

## Additional details

### Input File formats

#### Initial Ranking file

Example `initial-ranking.csv` file content:

```csv
,Lastname1,Name1,NumericRanking1,
,Lastname2,Name2,NumericRanking2,
...
,LastnameN,NameN,NumericRankingN,
```

Please note:

- The first column is blank: The line starts with `;`.
- It could have more than 4 columns, but they will be ignored by the process.
- `NumericRanking` column should be an integer number.

#### Tournament file

Starting with the filename, the tournament files should have a specific
convention: `yyyyMMdd-sameDayCounter-AnyName.txt`.

Example tournament filename: `20231122-1-U1500.txt` where:

- `20231122` is the date when the tournament were held. In this example, `November 22nd, 2023`.
- `1` is the counter of tournament at the same date. In this example, the `1` means that this was the `first tournament`
  held on November 22nd, 2023. If it would be another tournament in the same date, that second file would have a `2` in
  this section.
- `U1500` is the name or description of the tournament. In this example, it is described as an U1500 tournament, but it
  could be any related string. _This is not associated to any validation process, it is just informative._

Example tournament file content:

```txt
Any tournament name
Any organizer name
#Jugadores
Lastname1, Name1
Lastname2, Name2
Lastname3, Name3
Lastname4, Name4
#Partidos
##Todos
Lastname1, Name1, 3, Lastname2, Name2, 2
Lastname3, Name3, 3, Lastname4, Name4, 2
Lastname1, Name1, 3, Lastname3, Name3, 2
Lastname2, Name2, 3, Lastname4, Name4, 2
```

Please note:

- `Any tournament name` will be used as the tournament name in the ranking data, just for informative purposes.
- `Any organizer name` will be used as the organizer name in the ranking data, just for informative purposes.
- `#Partidos` is the third important separator.
- After the `#Partidos` separator, any line that start with `#` will be discarded as a comment.
- After the `#Partidos` separator, any line that don't start with `#` will be parsed as a **match**.
- A **match** line is treated as a comma separated string conformed by 6 columns.
    - The columns are:
        - Player A lastname: For example `Lastname1`.
        - Player A name: For example `Name1`
        - Player A winning sets count: For example `3`
        - Player B lastname: For example `Lastname2`
        - Player B name: For example `Name2`
        - Player B winning sets count: For example `2`
    - The program validates the winning sets correctness at runtime, e.g. the sum of both values should be `3`, `5`
      or `7`.

### Output File formats

#### Ranking Table JSON file

Example `rankings.json` file content:

```json
{
  "date": "2024-11-13T04:30:19.874488817",
  "rankings": [
    {
      "id": "2cdede0d-8d36-4c09-9fa0-9e1a0f22d66b",
      "updatedAt": "2024-04-12",
      "lastMatchID": null,
      "player": {
        "id": "2cd9b5d3-b255-4a0b-ad3e-6cabe6b9fdca",
        "name": "Name1",
        "lastname": "Lastname1"
      },
      "playerRankingBeforeMatch": null,
      "playerRankingAfterMatch": 1751,
      "currentRanking": false
    },
    {
      "id": "6cfc9b41-fa0f-4ee3-9ed4-26d0f43bedd8",
      "updatedAt": "2024-04-12",
      "lastMatchID": null,
      "player": {
        "id": "16edd935-a8c2-41dc-87ac-d3eca1b8aa89",
        "name": "Name2",
        "lastname": "Lastname2"
      },
      "playerRankingBeforeMatch": null,
      "playerRankingAfterMatch": 1522,
      "currentRanking": true
    },
    {
      "id": "fca34823-1d99-4b4d-9bb9-babf92ddf347",
      "updatedAt": "2024-04-12",
      "lastMatchID": null,
      "player": {
        "id": "2cd9b5d3-b255-4a0b-ad3e-6cabe6b9fdca",
        "name": "Name1",
        "lastname": "Lastname1"
      },
      "playerRankingBeforeMatch": null,
      "playerRankingAfterMatch": 1756,
      "currentRanking": true
    },
    ...
  ]
}
```


[Guayaquil City Table Tennis Club]: https://www.instagram.com/guayaquilcitytabletennisclub

[GCTTC Rankings Tool]: https://gitlab.com/gcttc/gcttc-rankings

[GCTTC Rankings Data]: https://gitlab.com/gcttc/gcttc-rankings-data

[GCTTC Rankings Web]: https://gitlab.com/gcttc/gcttc-rankings-web
